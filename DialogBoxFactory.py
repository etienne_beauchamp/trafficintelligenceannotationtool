from tkinter import *
from tkinter.colorchooser import askcolor
import tkinter.messagebox as tkMessageBox
import tkinter.filedialog as tkFileDialog
import tkinter.simpledialog as tkSimpleDialog
from tkinter.colorchooser import askcolor
from PIL import ImageTk, Image
import tkinter.filedialog as tkFileDialog
import os, time
import tkinter.ttk as ttk

class SimpleDialog:

    def __init__(self, master,
                 text='', buttons=[], default=None, cancel=None,
                 title=None, class_=None, imagePath=None):
        if class_:
            self.root = tkSimpleDialog.Toplevel(master, class_=class_)
        else:
            self.root = tkSimpleDialog.Toplevel(master)
        if title:
            self.root.title(title)
            self.root.iconname(title)
        if imagePath:
            self.logo = ImageTk.PhotoImage(Image.open(imagePath))
            w1 = Label(self.root, image=self.logo).pack(side="right")
        self.message = Message(self.root, text=text, aspect=400)
        self.message.pack(expand=1, fill=BOTH)
        self.frame = Frame(self.root)
        self.frame.pack()
        self.num = default
        self.cancel = cancel
        self.default = default
        self.root.bind('<Return>', self.return_event)
        for num in range(len(buttons)):
            s = buttons[num]
            b = Button(self.frame, text=s,
                       command=(lambda self=self, num=num: self.done(num)))
            if num == default:
                b.config(relief=RIDGE, borderwidth=8)
            b.pack(side=LEFT, fill=BOTH, expand=1)
        self.root.protocol('WM_DELETE_WINDOW', self.wm_delete_window)
        self._set_transient(master)

    def _set_transient(self, master, relx=0.5, rely=0.3):
        widget = self.root
        widget.withdraw() # Remain invisible while we figure out the geometry
        widget.transient(master)
        widget.update_idletasks() # Actualize geometry information
        if master.winfo_ismapped():
            m_width = master.winfo_width()
            m_height = master.winfo_height()
            m_x = master.winfo_rootx()
            m_y = master.winfo_rooty()
        else:
            m_width = master.winfo_screenwidth()
            m_height = master.winfo_screenheight()
            m_x = m_y = 0
        w_width = widget.winfo_reqwidth()
        w_height = widget.winfo_reqheight()
        x = m_x + (m_width - w_width) * relx
        y = m_y + (m_height - w_height) * rely
        if x+w_width > master.winfo_screenwidth():
            x = master.winfo_screenwidth() - w_width
        elif x < 0:
            x = 0
        if y+w_height > master.winfo_screenheight():
            y = master.winfo_screenheight() - w_height
        elif y < 0:
            y = 0
        widget.geometry("+%d+%d" % (x, y))
        widget.deiconify() # Become visible at the desired location

    def go(self):
        self.root.wait_visibility()
        self.root.grab_set()
        self.root.mainloop()
        try:
            self.root.destroy()
            return self.num
        except:
            pass

    def return_event(self, event):
        if self.default is None:
            self.root.bell()
        else:
            self.done(self.default)

    def wm_delete_window(self):
        if self.cancel is None:
            self.root.bell()
        else:
            self.done(self.cancel)

    def done(self, num):
        self.num = num
        self.root.quit()


class ProgressBarBox:

    def __init__(self, master,
                 text=None, 
                 title=None,
                 determinateMode=True, 
                 class_=None,
                 time = None,
                 finishText = 'Job finished' ):
        if class_:
            self.root = tkSimpleDialog.Toplevel(master, class_=class_)
        else:
            self.root = tkSimpleDialog.Toplevel(master)
        if title:
            self.root.title(title)
            self.root.iconname(title)
        self.master = master
        row = Frame(self.root)
        self.finaltime = time if time is not None else None
        self.text = Label(row, text=text if text is not None else '', anchor='w')
        self.pourcent = Label(row, text='0% -', width=10, anchor='e')
        row.pack(side=TOP, fill=X, padx=5, pady=5)
        self.text.pack(side = LEFT, fill=BOTH, padx=5, pady=5)
        self.pourcent.pack(side = RIGHT, fill=BOTH, padx=5, pady=5)
        row = Frame(self.root)
        self.value = 0
        self.loadingPhase = 1
        self.bar = ttk.Progressbar(row, orient = HORIZONTAL, mode='determinate' if determinateMode else 'indeterminate', maximum = time if determinateMode and time is not None else 100.)
        row.pack(side=TOP, fill=X, padx=5, pady=5)
        self.bar.pack(fill=BOTH, padx=5, pady=5)
        self.finishText = finishText 
        self.root.protocol('WM_DELETE_WINDOW', self.wm_delete_window)

    def _set_transient(self, master, relx=0.5, rely=0.3):
        widget = self.root
        widget.withdraw() # Remain invisible while we figure out the geometry
        widget.transient(master)
        widget.update_idletasks() # Actualize geometry information
        if master.winfo_ismapped():
            m_width = master.winfo_width()
            m_height = master.winfo_height()
            m_x = master.winfo_rootx()
            m_y = master.winfo_rooty()
        else:
            m_width = master.winfo_screenwidth()
            m_height = master.winfo_screenheight()
            m_x = m_y = 0
        w_width = widget.winfo_reqwidth()
        w_height = widget.winfo_reqheight()
        x = m_x + (m_width - w_width) * relx
        y = m_y + (m_height - w_height) * rely
        if x+w_width > master.winfo_screenwidth():
            x = master.winfo_screenwidth() - w_width
        elif x < 0:
            x = 0
        if y+w_height > master.winfo_screenheight():
            y = master.winfo_screenheight() - w_height
        elif y < 0:
            y = 0
        widget.geometry("+%d+%d" % (x, y))
        widget.deiconify() # Become visible at the desired location
        
    def __getitem__(self, key):
        if key == 'label':
            return self.text['text']
        if key == 'value':
            return self.value*100/(self.bar['maximum'] if self.finaltime is None else self.finaltime)
        raise IndexError("You can only use 'label' or 'value'")
            
    def __setitem__(self, key, value):
        if key == 'label':
            self.text['text'] = value
        elif key == 'value':
            if value<0 or value>100 :
                raise ValueError("Value must be between 0. and 100.")
            self.bar.step(value - self.value)
            self.value = value*(self.bar['maximum'] if self.finaltime is None else self.finaltime)/100
            self.pourcent['text'] = '{}% {}'.format(int(self.value*100/(self.bar['maximum'] if self.finaltime is None else self.finaltime)), '-\\|/'[self.loadingPhase])
            self.loadingPhase = (self.loadingPhase+1)%4
            self.bar.update()
        else:
            raise IndexError("You can only use 'label' or 'value'")
        if self.value >=(self.bar['maximum'] if self.finaltime is None else self.finaltime) and key == 'value': 
            self.done()

    def step(self):
        if self.value + (time.time() - self.lastTime)*1000 >= (self.bar['maximum'] if self.finaltime is None else self.finaltime) : 
            self.done()
        else:
            self.bar.step((time.time() - self.lastTime)*1000)
            self.bar.update()
            self.value += (time.time() - self.lastTime)*1000
            self.pourcent['text'] = '{}% {}'.format(int(self.value*100/(self.bar['maximum'] if self.finaltime is None else self.finaltime)), '-\\|/'[self.loadingPhase])
            self.loadingPhase = (self.loadingPhase+1)%4
            self.lastTime = time.time()
            if self.finaltime is not None:
                self.start()

    def start(self):
        self.root.after(50, self.step)

    def go(self):
        self._set_transient(self.master)
        if self.finaltime is not None :
            self.lastTime = time.time()
            self.start()
        

    def return_event(self, event):
        self.root.bell()

    def wm_delete_window(self):
        self.root.bell()

    def done(self):
        self.root.destroy()
        tkMessageBox.showinfo("Information", self.finishText, parent = self.master)
        
    def cancel(self):
        self.root.destroy()
        tkMessageBox.showinfo("Information", 'Cancelled', parent = self.master)

class ChoiceDialog:

    def __init__(self, master,
                 text='', choices=[], CancelCode=None,
                 title=None, class_=None):
        if class_:
            self.root = tkSimpleDialog.Toplevel(master, class_=class_)
        else:
            self.root = tkSimpleDialog.Toplevel(master)
        if title:
            self.root.title(title)
            self.root.iconname(title)
        self.message = Message(self.root, text=text, aspect=400)
        self.message.pack(expand=1, fill=BOTH)
        self.frame = Frame(self.root)
        self.num = StringVar()
        self.num.set(str(choices[0]))
        OptionMenu(self.frame,self.num,*choices).pack()
        self.frame.pack()
        self.cancel = CancelCode
        self.root.bind('<Return>', self.return_event)
        Button(self.frame, text="Confirm",
                       command=(lambda self=self, num=self.num: self.done(self.num.get()))).pack(side=LEFT, fill=BOTH, expand=1)
        Button(self.frame, text="Cancel",
                       command=(lambda self=self, num=self.num: self.done(self.cancel))).pack(side=LEFT, fill=BOTH, expand=1)
    
        self.root.protocol('WM_DELETE_WINDOW', self.wm_delete_window)
        self._set_transient(master)


    def _set_transient(self, master, relx=0.5, rely=0.3):
        widget = self.root
        widget.withdraw() # Remain invisible while we figure out the geometry
        widget.transient(master)
        widget.update_idletasks() # Actualize geometry information
        if master.winfo_ismapped():
            m_width = master.winfo_width()
            m_height = master.winfo_height()
            m_x = master.winfo_rootx()
            m_y = master.winfo_rooty()
        else:
            m_width = master.winfo_screenwidth()
            m_height = master.winfo_screenheight()
            m_x = m_y = 0
        w_width = widget.winfo_reqwidth()
        w_height = widget.winfo_reqheight()
        x = m_x + (m_width - w_width) * relx
        y = m_y + (m_height - w_height) * rely
        if x+w_width > master.winfo_screenwidth():
            x = master.winfo_screenwidth() - w_width
        elif x < 0:
            x = 0
        if y+w_height > master.winfo_screenheight():
            y = master.winfo_screenheight() - w_height
        elif y < 0:
            y = 0
        widget.geometry("+%d+%d" % (x, y))
        widget.deiconify() # Become visible at the desired location

    def go(self):
        self.root.wait_visibility()
        self.root.grab_set()
        self.root.mainloop()
        try:
            self.root.destroy()
            return self.num
        except:
            pass

    def return_event(self, event):
        if self.cancel is None:
            self.root.bell()
        else:
            self.done(self.cancel)
    def wm_delete_window(self):
        if self.cancel is None:
            self.root.bell()
        else:
            self.done(self.cancel)

    def done(self, num):
        self.num = num
        self.root.quit()



#Validators are functions that takes a string and return None if the string is valid and a message error if not.
#Types only 'string', 'int' and 'float' can be treated
class MultiInputDialog:

    def __init__(self, master,
                 labels=[], types=[],validators=[], CancelCode=None,
                 title=None, class_=None):
        if class_:
            self.root = tkSimpleDialog.Toplevel(master, class_=class_)
        else:
            self.root = tkSimpleDialog.Toplevel(master)
        if title:
            self.root.title(title)
            self.root.iconname(title)
        self.Widgets=[]
        self.labels=labels
        self.types=types
        self.validators=validators
            
        for i,l in enumerate(self.labels):
            if self.types[i]=='string':
                row = Frame(self.root)
                lab = Label(row, width=15, text=l, anchor='w')
                def tmp(idx):
                    def tmp2():
                        value = tkSimpleDialog.askstring('Enter value for '+self.labels[idx],'Enter an string', initialvalue='')
                        if value is not None:
                            message = self.validators[idx](str(value))
                        else:
                            message = self.validators[idx](str(self.Widgets[idx].get()))
                        if message is None:
                            self.Widgets[idx]['text']=str(value)
                            return True
                        else:
                            tkMessageBox.showerror('Error', message )
                            return False
                    return tmp2
                self.Widgets.append(Label(row, width=20))
                row.pack(side=TOP, fill=X, padx=5, pady=5)
                lab.pack(side=LEFT, padx=5, pady=5)
                self.Widgets[-1].pack(side=RIGHT, expand=YES, fill=X, padx=5, pady=5)
                Button(row, text="Entrer a text", command=tmp(i), width=20).pack(side=RIGHT, padx=5, pady=5)
            elif self.types[i]=='fread':
                row = Frame(self.root)
                lab = Label(row, width=15, text=l, anchor='w')
                def tmp(idx):
                    def tmp2():
                        value = tkFileDialog.askopenfilename(title = 'Open a file for '+self.labels[idx], initialdir = os.getcwd(), filetypes = self.validators[idx])
                        if len(value)>0:
                            self.Widgets[idx]['text']=str(value)
                            return True
                        else:
                            tkMessageBox.showerror('Error', 'No file found' )
                            return False
                    return tmp2
                self.Widgets.append(Label(row, width=20))
                row.pack(side=TOP, fill=X, padx=5, pady=5)
                lab.pack(side=LEFT, padx=5, pady=5)
                self.Widgets[-1].pack(side=RIGHT, expand=YES, fill=X, padx=5, pady=5)
                Button(row, text="Choose an existing file", command=tmp(i), width=20).pack(side=RIGHT, padx=5, pady=5)
            elif self.types[i]=='fwrite':
                row = Frame(self.root)
                lab = Label(row, width=15, text=l, anchor='w')
                def tmp(idx):
                    def tmp2():
                        value = tkFileDialog.asksaveasfilename(title = 'Open a file for '+self.labels[idx], initialdir = os.getcwd(), filetypes = self.validators[idx])
                        if len(value)>0:
                            self.Widgets[idx]['text']=str(value)
                            return True
                        else:
                            tkMessageBox.showerror('Error', 'No file found' )
                            return False
                    return tmp2
                self.Widgets.append(Label(row, width=20))
                row.pack(side=TOP, fill=X, padx=5, pady=5)
                lab.pack(side=LEFT, padx=5, pady=5)
                self.Widgets[-1].pack(side=RIGHT, expand=YES, fill=X, padx=5, pady=5)
                Button(row, text="Choose an file", command=tmp(i), width=20).pack(side=RIGHT, padx=5, pady=5)
            elif self.types[i]=='int' :
                row = Frame(self.root)
                lab = Label(row, width=15, text=l, anchor='w')
                def tmp(idx):
                    def tmp2():
                        value = tkSimpleDialog.askinteger('Enter value for '+self.labels[idx],'Enter an int', initialvalue=0)
                        if value is not None:
                            message = self.validators[idx](str(value))
                        else:
                            try :
                                message = self.validators[idx](str(self.Widgets[idx]['text']))
                            except ValueError:
                                message = 'Could not convert "'+str(self.Widgets[idx]['text'])+'" to an int.'
                        if message is None:
                            self.Widgets[idx]['text'] = str(value)
                            return True
                        else:
                            tkMessageBox.showerror('Error', message )
                            return False
                    return tmp2
                self.Widgets.append(Label(row, text='0', width=20))
                row.pack(side=TOP, fill=X, padx=5, pady=5)
                lab.pack(side=LEFT, padx=5, pady=5)
                self.Widgets[-1].pack(side=RIGHT, expand=YES, fill=X, padx=5, pady=5)
                Button(row, text="Entrer an int", command=tmp(i), width=20).pack(side=RIGHT, padx=5, pady=5)
            elif self.types[i]=='color' :
                row = Frame(self.root)
                lab = Label(row, width=15, text=l, anchor='w')
                def tmp(idx):
                    def tmp2():
                        value = askcolor(title = 'Enter value for '+self.labels[idx])
                        if value[1] is not None:
                            message = self.validators[idx](str(value))
                        else:
                            message = self.validators[idx](str(self.Widgets[idx].get()))
                        if message is None:
                            self.Widgets[idx]['bg']=value[1]
                            return True
                        else:
                            tkMessageBox.showerror('Error', message )
                            return False
                    return tmp2
                self.Widgets.append(Label(row, bg='white', width=20))
                row.pack(side=TOP, fill=X, padx=5, pady=5)
                lab.pack(side=LEFT, padx=5, pady=5)
                self.Widgets[-1].pack(side=RIGHT, expand=YES, fill=X, padx=5, pady=5)
                Button(row, text="Choose a color", command=tmp(i), width=20).pack(side=RIGHT, padx=5, pady=5)
            elif self.types[i]=='float':
                row = Frame(self.root)
                lab = Label(row, width=15, text=l, anchor='w')
                def tmp(idx):
                    def tmp2():
                        value = tkSimpleDialog.askfloat('Enter value for '+self.labels[idx],'Enter a float',initialvalue=0.0)
                        if value is not None:
                            message = self.validators[idx](str(value))
                        else:
                            try :
                                message = self.validators[idx](str(self.Widgets[idx]['text']))
                            except ValueError:
                                message = 'Could not convert "'+str(self.Widgets[idx]['text'])+'" to a float.'
                        if message is None:
                            self.Widgets[idx]['text'] = str(value)
                            return True
                        else:
                            tkMessageBox.showerror('Error', message )
                            return False
                    return tmp2
                self.Widgets.append(Label(row, text='0.0', width=20))
                row.pack(side=TOP, fill=X, padx=5, pady=5)
                lab.pack(side=LEFT, padx=5, pady=5)
                self.Widgets[-1].pack(side=RIGHT, expand=YES, fill=X, padx=5, pady=5)
                Button(row, text="Entrer a float", command=tmp(i), width=20).pack(side=RIGHT, padx=5, pady=5)
            else:
                raise ValueError('Can not be treated : '+self.types[i])
                
        
        
        
        self.cancel = CancelCode
        self.root.bind('<Return>', self.return_event)
        Button(self.root, text="Confirmer",
                       command=(lambda self=self: self.done(True))).pack(side=LEFT, fill=BOTH, expand=1)
        Button(self.root, text="Annuler",
                       command=(lambda self=self: self.done(False))).pack(side=LEFT, fill=BOTH, expand=1)
    
        self.root.protocol('WM_DELETE_WINDOW', self.wm_delete_window)
        self._set_transient(master)


    '''def showError(self, message):
        print (self.ent[0]['validatecommand'])
        print( self.ent[1]['validatecommand'])'''
        

    def _set_transient(self, master, relx=0.5, rely=0.3):
        widget = self.root
        widget.withdraw() # Remain invisible while we figure out the geometry
        widget.transient(master)
        widget.update_idletasks() # Actualize geometry information
        if master.winfo_ismapped():
            m_width = master.winfo_width()
            m_height = master.winfo_height()
            m_x = master.winfo_rootx()
            m_y = master.winfo_rooty()
        else:
            m_width = master.winfo_screenwidth()
            m_height = master.winfo_screenheight()
            m_x = m_y = 0
        w_width = widget.winfo_reqwidth()
        w_height = widget.winfo_reqheight()
        x = m_x + (m_width - w_width) * relx
        y = m_y + (m_height - w_height) * rely
        if x+w_width > master.winfo_screenwidth():
            x = master.winfo_screenwidth() - w_width
        elif x < 0:
            x = 0
        if y+w_height > master.winfo_screenheight():
            y = master.winfo_screenheight() - w_height
        elif y < 0:
            y = 0
        widget.geometry("+%d+%d" % (x, y))
        widget.deiconify() # Become visible at the desired location

    def go(self):
        self.root.wait_visibility()
        self.root.grab_set()
        self.root.mainloop()
        try:
            self.root.destroy()
            return self.num
        except:
            pass

    def return_event(self, event):
        if self.cancel is None:
            self.root.bell()
        else:
            self.done(self.cancel)

    def wm_delete_window(self):
        if self.cancel is None:
            self.root.bell()
        else:
            self.done(self.cancel)

    def done(self, Confirm):
        Ok = True
        if not Confirm:
            self.num = self.cancel
        else:
            self.num={}
            message = ''
            for i,(l,t,v) in enumerate(zip(self.labels,self.types,self.validators)) :
                try:
                    add = v(self.Widgets[i]['bg'] if t=='color' else self.Widgets[i]['text']) if t!='fread' and t!='fwrite' else ('No file found' if len(self.Widgets[i]['text'])==0 else None)
                except ValueError:
                    add = 'Could not convert data to an integer or float.'
                if add is not None:
                    message+=str(i+1)+') '+add+'\n'
                    Ok = False
                if t=='int' and Ok:
                    self.num[l]=int(self.Widgets[i]['text'])
                elif t=='string' and Ok:
                    self.num[l]=str(self.Widgets[i]['text'])
                elif t=='color' and Ok:
                    self.num[l]=self.Widgets[i]['bg']
                elif t=='float' and Ok:
                    self.num[l]=float(self.Widgets[i]['text'])
                elif t=='fread' and Ok:
                    self.num[l]=float(self.Widgets[i]['text'])
            
        if Ok:
            self.root.quit()    
        else:
            tkMessageBox.showerror('Error', message )    
        



if __name__ == '__main__':

    def test():
        root = Tk()
        def doit(root=root):
            d = SimpleDialog(root,
            
                         text="This is a Tool for the Traffic Intelligence project. This software project provides a set of tools developed by Nicolas Saunier and his collaborators for transportation data processing, in particular road traffic, motorized and non-motorized. The project consists in particular in tools for the most typical transportation data type, trajectories, i.e. temporal series of positions. The original work targeted automated road safety analysis using video sensors." ,
                         buttons=["Ok"],
                         default=0,
                         cancel=2,
                         title="Test Dialog",imagePath="Images/about.jpeg")
            print(d.go())
            
        def doit2(root=root):
            d = ChoiceDialog(root,
                 text='Vos choix ?', choices=['Ali', 'Est', 'Vetu'], 
                 title='Je fais le malin')
            print(d.go())
        
        def doit3(root=root):
            d = MultiInputDialog(root,
                 labels=['a text', 'an int', 'a float', 'a color', 'open a file', 'save a file'], 
                 types=['string','int','float', 'color', 'fread', 'fwrite'], 
                 validators=[   (lambda x : None if len(x)==15 else '15 char' ), 
                                (lambda x : None if int(x)<=5 and int(x)>=2 else 'between 2 et 5'), 
                                (lambda x : None if len(str(x))==3 else '3 char'), 
                                (lambda x : None if x is not None else 'a valid color please'), 
                                [("configuration file","*.ini"),("configuration file","*.cfg")], 
                                [("configuration file","*.ini"),("configuration file","*.cfg")]
                             ], 
                 CancelCode=None,
                 title='A test Tool')
            print(d.go())
            
        def doit4(root=root):    
            d = ProgressBarBox(root, title = 'A bar dialog', text = 'Please wait...', determinateMode = 0)
            d.go()
            for i in range(0, 10):
                d['value']+=10
                time.sleep(2)
        t = Button(root, text='Test', command=doit)
        t.pack()
        Button(root, text='Test2', command=doit2).pack()
        Button(root, text='Test3', command=doit3).pack()
        Button(root, text='Test4', command=doit4).pack()
        q = Button(root, text='Quit', command=t.quit)
        q.pack()
        t.mainloop()

    test()
