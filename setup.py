from setuptools import setup
__version__ = '0.1'
with open("README.md", "r") as fp:
    long_description = fp.read()

with open('requirements.txt') as f:
    require_pip = f.read().splitlines()


setup(name="TrafficIntelligenceAnnotationTool",
      version=__version__,
      author="Wendlasida Tertius OUEDRAOGO",
      author_email="ouedraogo.tertius@gmail.com",
      url="https://bitbucket.org/Wendlasida/trafficintelligenceannotationtool/",
      download_url = 'https://bitbucket.org/Wendlasida/trafficintelligenceannotationtool/get/2fa572ce7cb3.zip',
      install_requires = require_pip,
      description="A tool for trafficIntelligence",
      long_description=long_description,
      classifiers=(
        "Programming Language :: Python :: 3 :: Only",
        "Operating System :: OS Independent",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Visualization",
        ),
    )
    
