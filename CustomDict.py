import undoredo

class UndoableDict(object):
    def __init__(self, InitialDict={}):
        self.undo_mgr = undoredo.UndoManager()
        self.value = dict(InitialDict)
        
    @undoredo.undoable
    def __setitem__(self,key,value):
        self.value[key]=value
        
    @undoredo.undoable
    def deleteManyValues(self, keys):
        for key in keys:
            del self.value[key]
    
    
    @undoredo.undoable
    def __delitem__(self, key):
        del self.value[key]

    def __getitem__(self, key):
        return self.value[key]
        
    def __contains__(self, item):
        return item in self.value
    
    def __len__(self):
        return len(self.value.keys())
    
    def keys(self):
        return self.value.keys()
    
    def values(self):
        return self.value.values()
        
    def do(self, command):
        return self.undo_mgr.do(command)

    def undo(self):
        return self.undo_mgr.undo()

    def redo(self):
        return self.undo_mgr.redo()

    def copy(self):
        return UndoableDict(self.value)

    def can_undo(self):
        return self.undo_mgr.can_undo()
        
    def can_redo(self):
        return self.undo_mgr.can_redo()

    def restore(self, counter):
        self.value = counter.value

