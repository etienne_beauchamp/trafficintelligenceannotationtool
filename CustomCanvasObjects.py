import sys
import tkinter as tk
import CustomDict
import numpy
import copy
import abc
from scipy.interpolate import interp1d
from scipy import arange, array, exp

def extrap1d(interpolator, defaut = 0):
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if len(xs)<2:
            return defaut
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return pointwise(xs) if type(xs) is int or type(xs) is float else [pointwise(x) for x in list(xs)]

    return ufunclike

from trafficintelligence import moving

class ObjectInTime(object):
    def __init__(self):
        self.current_frame = -1 #The Number of the frame displayed 
        self.UpdatedPosition=CustomDict.UndoableDict() #Temporary positions : What we really modify
        
    def updateSize(self, event):
        self.setToFrame(self.current_frame)
        
    def HasAPoint(self):
        return self.current_frame in self.UpdatedPosition
        
            
    def canUndoPosition(self):
        return self.UpdatedPosition.can_undo()

    def canRedoPosition(self):
        return self.UpdatedPosition.can_redo()

    def undoPosition(self):
        if self.UpdatedPosition.can_undo():
            frameNumber=self.UpdatedPosition.undo_mgr._undo_stack[-1].args[0]
            self.UpdatedPosition.undo()
            self.setToFrame(frameNumber)

    def redoPosition(self):
        if self.UpdatedPosition.can_redo():
            frameNumber=self.UpdatedPosition.undo_mgr._redo_stack[-1].args[0]
            self.UpdatedPosition.redo()
            self.setToFrame(frameNumber)
            
    @abc.abstractmethod
    def setToFrame(self, frameNumber):
        ...
    
    
    

#Dragable points and Position updates management
class EditablePointWithTimeManagement(ObjectInTime):
    def __init__(self, VideoTool, color , textcolor, xpos=0, ypos=0 , idobj='-1' , size=20, invHomography=None, Bind=True, Display=True, newCamera=None, interpkind = 'slinear'):
        ObjectInTime.__init__(self)
        self.size=size #Our circle diameter in pixel
        self.canvas = VideoTool.Zoomable.canvas #We keep an eye on our canas
        self.ignore_move= False #If we must ignore the cursor moves
        self.VideoTool = VideoTool 
        self.idtext = idobj
        self.initX = xpos
        self.initY = ypos
        self.interpkind = interpkind
        self.OvalItem= VideoTool.Zoomable.canvas.create_oval(xpos-size/2,ypos-size/2,
            xpos+size/2, ypos+size/2,fill=color,state=tk.NORMAL if Display else tk.HIDDEN)
        self.NameTextItem= VideoTool.Zoomable.canvas.create_text(xpos,ypos,text=idobj,fill=textcolor,state=tk.NORMAL if Display else tk.HIDDEN )
        self.precLink = None
        VideoTool.Zoomable.canvas.bind('<Configure>',(lambda x : self.setToFrame(self.current_frame)),'+')
        VideoTool.Zoomable.canvas.bind('<MouseWheel>', self.updateSize, '+')
        VideoTool.Zoomable.canvas.bind('<Button-5>', self.updateSize,'+')
        VideoTool.Zoomable.canvas.bind('<Button-4>', self.updateSize,'+')
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Button-1>', self.select )
        VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<Button-1>', self.select)
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Control-1>', self.selectKeep)
        VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<Control-1>', self.selectKeep)
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Button-3>', self.select )
        VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<Button-3>', self.select)
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Control-3>', self.selectKeep)
        VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<Control-3>', self.selectKeep)
        if Bind:
            VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Button1-Motion>', self.move)
            VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<ButtonRelease-1>', self.release)
            VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<Button1-Motion>', self.move)
            VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<ButtonRelease-1>', self.release)
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem,'<Button-3>', VideoTool.B3MouseMenuContent, '+')
        VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem,'<Button-3>', VideoTool.B3MouseMenuContent, '+')
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem,'<Control-3>', VideoTool.B3MouseMenuContent, '+')
        VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem,'<Control-3>', VideoTool.B3MouseMenuContent, '+')
        self.move_flag = False
        self.invHomography = invHomography
        self.newCamera = newCamera
        self.color=color
        self.textcolor=textcolor
        self.lastImscale = self.VideoTool.Zoomable.imscale
        self.selected = False
        
    
    def select(self, event):
            self.VideoTool.SelectPoints([self.OvalItem])
    
    def selectKeep(self, event):
            self.VideoTool.SelectPoints([self.OvalItem],True)
            
    def setSelect(self,select):
            self.selected = select
            if select:
                self.canvas.itemconfig(self.OvalItem, outline='blue', width=5)
                self.canvas.tag_raise(self.OvalItem)
                self.canvas.tag_raise(self.NameTextItem)
            else:
                self.canvas.itemconfig(self.OvalItem,outline='black', width=1)
    
    def setColor(self,color,Cercle=True):
        if Cercle:
            self.canvas.itemconfig(self.OvalItem,fill=color)
            self.canvas.itemconfig(self.precLink, fill=color)
            self.color=color
        else:
            self.canvas.itemconfig(self.NameTextItem,fill=color)
            self.textcolor=color


    def getTimeIntervalPositionsAndVelocities(self):
        positions = moving.Trajectory()
        velocities = moving.Trajectory()
        timeInterval = moving.TimeInterval(min(self.UpdatedPosition.keys()),max(self.UpdatedPosition.keys()))
        keys = sorted(self.UpdatedPosition.keys())
        if self.invHomography is not None:
            homographie = numpy.linalg.inv(self.invHomography)
        else:
            homographie = None
        if self.newCamera is not None :
            nCamera = numpy.linalg.inv(self.newCamera)
        else :
            nCamera = None
        posx = interp1d(list(self.UpdatedPosition.keys()), [p[0] for p in self.UpdatedPosition.values()], kind=self.interpkind)(list(range(timeInterval.first, timeInterval.last+1)))
        posy = interp1d(list(self.UpdatedPosition.keys()), [p[1] for p in self.UpdatedPosition.values()], kind=self.interpkind)(list(range(timeInterval.first, timeInterval.last+1)))
        for i in range(len(posx)):
            try:
                vx, vy = (posx[i+1] - posx[i])/2, (posy[i+1] - posy[i])/2
            except:
                vx, vy = 0., 0.
            positions.addPositionXY(posx[i], posy[i])
            velocities.addPositionXY(vx, vy)
        return timeInterval, positions.newCameraProject(nCamera).homographyProject(homographie), velocities.newCameraProject(nCamera).homographyProject(homographie)

    def getColors(self):
        return (self.color, self.textcolor)

    def move(self, event):
        self.VideoTool.onPlay=False
        x,y=self.canvas.coords(self.NameTextItem)
        new_xpos, new_ypos = event.x, event.y
        widthi, heighti = self.VideoTool.Zoomable.image.size
        widthi, heighti = int(self.VideoTool.Zoomable.imscale[0] * widthi), int(self.VideoTool.Zoomable.imscale[1] * heighti)
        if self.ignore_move:
            if not ((x+new_xpos-self.mouse_xpos)<0 or (x+new_xpos-self.mouse_xpos)>widthi or (y+new_ypos-self.mouse_ypos)<0 or (y+new_ypos-self.mouse_ypos)>heighti) :
                    self.ignore_move= False
            else:
                pass
        elif self.move_flag:
            if (x+new_xpos-self.mouse_xpos)<0 or (x+new_xpos-self.mouse_xpos)>widthi or (y+new_ypos-self.mouse_ypos)<0 or (y+new_ypos-self.mouse_ypos)>heighti :
                self.ignore_move= True
            else :
                self.canvas.move(self.OvalItem,
                    new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
                self.canvas.move(self.NameTextItem,
                    new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
                if self.precLink is None :
                    frameList = [f for f in self.UpdatedPosition.keys() if f<self.current_frame]
                    if frameList != []:
                        precFrameNumber = max(frameList)
                        xpos, ypos = self.UpdatedPosition[precFrameNumber]
                    else:
                        precFrameNumber = None
                    if precFrameNumber is not None:
                        self.precLink = self.canvas.create_line(xpos*self.VideoTool.Zoomable.imscale[0], ypos*self.VideoTool.Zoomable.imscale[1], x*self.VideoTool.Zoomable.imscale[0],y*self.VideoTool.Zoomable.imscale[1], fill=self.color, state=tk.NORMAL, width=5)
                else:
                    xpos, ypos, _, _ = self.canvas.coords(self.precLink)
                    self.canvas.coords(self.precLink, xpos, ypos, x, y )

    
            self.mouse_xpos = new_xpos
            self.mouse_ypos = new_ypos
        else:
            self.move_flag = True
            self.canvas.tag_raise(self.OvalItem)
            self.canvas.tag_raise(self.NameTextItem)
            self.mouse_xpos = event.x
            self.mouse_ypos = event.y


    def release(self, event):
        self.move_flag = False
        self.ignore_move= False
        x, y=self.canvas.coords(self.NameTextItem)
        self.UpdatedPosition[self.current_frame]=(x/self.VideoTool.Zoomable.imscale[0] , y/self.VideoTool.Zoomable.imscale[1])


    def deleteFromCanvas(self):
        self.canvas.delete(self.OvalItem)
        self.canvas.delete(self.NameTextItem)
        if self.precLink is not None :
            self.canvas.delete(self.precLink)


    def setToFrame(self, frameNumber):
        self.current_frame=frameNumber
        widthi, heighti = self.VideoTool.Zoomable.image.size
        widthi, heighti = int(self.VideoTool.Zoomable.imscale[0] * widthi), int(self.VideoTool.Zoomable.imscale[1] * heighti)
        try:
            x,y=self.UpdatedPosition[frameNumber]
        except :
            if len(self.UpdatedPosition.value)>=2 :
                interp1x, interp1y = extrap1d(interp1d(list(self.UpdatedPosition.keys()), [p[0] for p in self.UpdatedPosition.values()], kind=self.interpkind)), extrap1d(interp1d(list(self.UpdatedPosition.keys()), [p[1] for p in self.UpdatedPosition.values()], kind=self.interpkind)) 
                p1x, p1y = interp1x(frameNumber), interp1y(frameNumber)
                x, y = 0 if p1x<0 else (p1x if p1x*self.VideoTool.Zoomable.imscale[0] < widthi  else widthi/self.VideoTool.Zoomable.imscale[0]) , 0 if p1y<0 else (p1y if p1y*self.VideoTool.Zoomable.imscale[1] < heighti else heighti/self.VideoTool.Zoomable.imscale[1])
            
            elif len(self.UpdatedPosition.value)==1:
                x,y = list(self.UpdatedPosition.values())[0]
            else:
                x,y = 0 if self.initX < 0 else (self.initX if self.initX*self.VideoTool.Zoomable.imscale[0] < widthi else widthi/self.VideoTool.Zoomable.imscale[0]) , 0 if self.initY < 0 else (self.initY if self.initY*self.VideoTool.Zoomable.imscale[1] < heighti else heighti/self.VideoTool.Zoomable.imscale[1])
            self.UpdatedPosition[frameNumber]=(x,y)
        self.canvas.coords(self.OvalItem,int(x*self.VideoTool.Zoomable.imscale[0]-self.size/2),int(y*self.VideoTool.Zoomable.imscale[1]-self.size/2), int(x*self.VideoTool.Zoomable.imscale[0]+self.size/2), int(y*self.VideoTool.Zoomable.imscale[1]+self.size/2)  )
        self.canvas.coords(self.NameTextItem,x*self.VideoTool.Zoomable.imscale[0],y*self.VideoTool.Zoomable.imscale[1] )
        frameList = [f for f in self.UpdatedPosition.keys() if f<frameNumber]
        if frameList != []:
            precFrameNumber = max(frameList)
            xpos, ypos = self.UpdatedPosition[precFrameNumber]
        else:
            precFrameNumber = None
        if self.precLink is None and precFrameNumber is not None:
            self.precLink = self.canvas.create_line(xpos*self.VideoTool.Zoomable.imscale[0], ypos*self.VideoTool.Zoomable.imscale[1], x*self.VideoTool.Zoomable.imscale[0],y*self.VideoTool.Zoomable.imscale[1], fill=self.color, state=tk.NORMAL, width=5)
        elif precFrameNumber is not None:
            self.canvas.coords(self.precLink, xpos*self.VideoTool.Zoomable.imscale[0], ypos*self.VideoTool.Zoomable.imscale[1], x*self.VideoTool.Zoomable.imscale[0], y*self.VideoTool.Zoomable.imscale[1] )
            
        
    

class FeaturePoint(EditablePointWithTimeManagement):
    def __init__(self, mouvingObj, Feature, VideoTool, color , textcolor, xpos=0, ypos=0 , idobj='-1' , size=20, invHomography=None, movable=True, newCamera=None ):
        EditablePointWithTimeManagement.__init__(self, VideoTool, color , textcolor, xpos, ypos, idobj, size, invHomography, Bind=False, Display=False, newCamera = newCamera)
        self.obj=mouvingObj #The movingObj the point represents
        self.feature=Feature
        self.UpdatedPosition=CustomDict.UndoableDict({i : j for i,j in zip(range(Feature.getFirstInstant(),Feature.getLastInstant()+1),Feature.positions.homographyProject(invHomography).newCameraProject(newCamera))}) 
        if movable:
            VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Button1-Motion>', self.move)
            VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<ButtonRelease-1>', self.release)
            VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<Button1-Motion>', self.move)
            VideoTool.Zoomable.canvas.tag_bind(self.NameTextItem, '<ButtonRelease-1>', self.release)
        self.show=True
            
    def saveData(self):
            positions = moving.Trajectory()
            for i in range(self.feature.getFirstInstant(),self.feature.getLastInstant()+1) :
                positions.addPositionXY(self.UpdatedPosition[i][0],self.UpdatedPosition[i][1])
            self.feature=self.obj.features[self.obj.features.index(self.feature)]
            if self.invHomography is not None:
                homographie = numpy.linalg.inv(self.invHomography)
            else:
                homographie = None
            if self.newCamera is not None :
                nCamera = numpy.linalg.inv(self.newCamera)
            else :
                nCamera = None
            self.obj.features[self.obj.features.index(self.feature)].positions= positions.newCameraProject(nCamera).homographyProject(homographie)
            self.UpdatedPosition=CustomDict.UndoableDict({i : j for i,j in zip(range(self.feature.getFirstInstant(),self.feature.getLastInstant()+1),self.feature.positions.homographyProject(self.invHomography).newCameraProject(self.newCamera))})
        
        
    def restoreData(self):
            self.UpdatedPosition=CustomDict.UndoableDict({i : j for i,j in zip(range(self.feature.getFirstInstant(),self.feature.getLastInstant()+1),self.feature.positions.homographyProject(self.invHomography))})
            self.setToFrame(self.current_frame)
        
    
    def setVisibility(self,show=True):
        self.show=show
        self.canvas.itemconfig(self.OvalItem,state=tk.NORMAL if show and self.current_frame in self.UpdatedPosition else tk.HIDDEN )
        self.canvas.itemconfig(self.NameTextItem,state=tk.NORMAL if show and self.current_frame in self.UpdatedPosition else tk.HIDDEN)
    
    def getVisibility(self):
            return self.show
        
    def toggleVisibility(self):
            self.setVisibility(not self.show)
    
    def move(self, event, transmit = True):
        if not self.isReallyVisible():
            return
        super(FeaturePoint, self).move(event)
        if transmit:
            self.VideoTool.BroadcastMotion(event,self)
     
    
    def release(self, event, transmit=True):
        super(FeaturePoint, self).release(event)
        if transmit:
            self.VideoTool.BroadcastRelease(event,self)
                
    def isReallyVisible(self):
            return self.current_frame in self.UpdatedPosition and self.show
    
    def setToFrame(self, frameNumber):
        self.current_frame=frameNumber
        if self.show:
            try:
                x,y=self.UpdatedPosition[frameNumber]
                self.canvas.itemconfig(self.OvalItem,state=tk.NORMAL)
                self.canvas.itemconfig(self.NameTextItem,state=tk.NORMAL)
                self.canvas.coords(self.OvalItem,int(x*self.VideoTool.Zoomable.imscale[0]-self.size/2),int(y*self.VideoTool.Zoomable.imscale[1]-self.size/2),int(x*self.VideoTool.Zoomable.imscale[0]+self.size/2),int(y*self.VideoTool.Zoomable.imscale[1]+self.size/2)  )
                self.canvas.coords(self.NameTextItem,x*self.VideoTool.Zoomable.imscale[0],y*self.VideoTool.Zoomable.imscale[1] )
            except :
                self.canvas.itemconfig(self.OvalItem,state=tk.HIDDEN)
                self.canvas.itemconfig(self.NameTextItem,state=tk.HIDDEN)
        else:
            self.canvas.itemconfig(self.OvalItem,state=tk.HIDDEN)
            self.canvas.itemconfig(self.NameTextItem,state=tk.HIDDEN)
            self.setSelect(False)


class EditableRectangle(object):
    def __init__(self, VideoTool, point_color = 'black', color = 'blue' , size = 10,  p1=(0, 0), p2=(20, 20), dashes = [3 , 2]):
        self.canvas = VideoTool.Zoomable.canvas #We keep an eye on our canas
        self.p1 = self.canvas.create_rectangle(int(p1[0]-size/2) , int(p1[1]-size/2) , int(p1[0]+size/2) , int(p1[1]+size/2), fill = point_color)
        self.p2 = self.canvas.create_rectangle(int(p2[0]-size/2) , int(p2[1]-size/2) , int(p2[0]+size/2) , int(p2[1]+size/2), fill = point_color)
        self.point_color = point_color
        self.color = color
        self.rect = self.canvas.create_rectangle(*p1 , *p2, outline = color, dash = dashes)
        self.ignore_move= False #If we must ignore the cursor moves
        self.move_flag = False
        self.VideoTool = VideoTool
        self.size = size
        VideoTool.Zoomable.canvas.tag_bind(self.p1, '<Button1-Motion>', self.resizeByItem( item = self.p1) )
        VideoTool.Zoomable.canvas.tag_bind(self.p1, '<ButtonRelease-1>', self.release)
        VideoTool.Zoomable.canvas.tag_bind(self.p2, '<Button1-Motion>', self.resizeByItem( item = self.p2) )
        VideoTool.Zoomable.canvas.tag_bind(self.p2, '<ButtonRelease-1>', self.release)
        VideoTool.Zoomable.canvas.tag_bind(self.rect, '<Button1-Motion>', self.move)
        VideoTool.Zoomable.canvas.tag_bind(self.rect, '<ButtonRelease-1>', self.release)
        
    def getColors(self):
        return (self.point_color, self.color)
        
    def setBoundings(self, p1 , p2):
        self.canvas.coords(self.p1, int(p1[0]-self.size/2) , int(p1[1]-self.size/2) , int(p1[0]+self.size/2) , int(p1[1]+self.size/2))
        self.canvas.coords(self.p2, int(p2[0]-self.size/2) , int(p2[1]-self.size/2) , int(p2[0]+self.size/2) , int(p2[1]+self.size/2))
        self.canvas.coords(self.rect, *p1, *p2)
        
    
    def getBoundings(self):
        x1, y1, x2, y2 = self.canvas.coords(self.rect)
        return ( (x1, y1) , (x2, y2) )
    
    
    def move(self, event):
        x1, y1, x2, y2 = self.canvas.coords(self.rect)
        new_xpos, new_ypos = event.x, event.y
        widthi, heighti = self.VideoTool.Zoomable.image.size
        widthi, heighti = int(self.VideoTool.Zoomable.imscale[0] * widthi), int(self.VideoTool.Zoomable.imscale[1] * heighti)
        if self.ignore_move:
            if not ((x1+new_xpos-self.mouse_xpos)<0 or (x1+new_xpos-self.mouse_xpos)>widthi or (y1+new_ypos-self.mouse_ypos)<0 or (y1+new_ypos-self.mouse_ypos)>heighti or (x2+new_xpos-self.mouse_xpos)<0 or (x2+new_xpos-self.mouse_xpos)>widthi or (y2+new_ypos-self.mouse_ypos)<0 or (y2+new_ypos-self.mouse_ypos)>heighti) :
                    self.ignore_move= False
            else:
                pass
        elif self.move_flag:
            if (x1+new_xpos-self.mouse_xpos)<0 or (x1+new_xpos-self.mouse_xpos)>widthi or (y1+new_ypos-self.mouse_ypos)<0 or (y1+new_ypos-self.mouse_ypos)>heighti or (x2+new_xpos-self.mouse_xpos)<0 or (x2+new_xpos-self.mouse_xpos)>widthi or (y2+new_ypos-self.mouse_ypos)<0 or (y2+new_ypos-self.mouse_ypos)>heighti :
                self.ignore_move= True
            else :
                self.canvas.move(self.rect,
                    new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
                self.canvas.move(self.p1,
                    new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
                self.canvas.move(self.p2,
                    new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)


            self.mouse_xpos = new_xpos
            self.mouse_ypos = new_ypos
        else:
            self.move_flag = True
            self.canvas.tag_raise(self.rect)
            self.canvas.tag_raise(self.p1)
            self.canvas.tag_raise(self.p2)
            self.mouse_xpos = event.x
            self.mouse_ypos = event.y
    
    def resizeByItem(self, event = None, item = None) :
        
        def resize(event):
            x1, y1, x2, y2 = self.canvas.coords(item)
            x1 = (x1 + x2)/2
            y1 = (y1 + y2)/2
            new_xpos, new_ypos = event.x, event.y
            widthi, heighti = self.VideoTool.Zoomable.image.size
            widthi, heighti = int(self.VideoTool.Zoomable.imscale[0] * widthi), int(self.VideoTool.Zoomable.imscale[1] * heighti)
            if self.ignore_move:
                if not ((x1+new_xpos-self.mouse_xpos)<0 or (x1+new_xpos-self.mouse_xpos)>widthi or (y1+new_ypos-self.mouse_ypos)<0 or (y1+new_ypos-self.mouse_ypos)>heighti ):
                        self.ignore_move= False
                else:
                    pass
            elif self.move_flag:
                if (x1+new_xpos-self.mouse_xpos)<0 or (x1+new_xpos-self.mouse_xpos)>widthi or (y1+new_ypos-self.mouse_ypos)<0 or (y1+new_ypos-self.mouse_ypos)>heighti :
                    self.ignore_move= True
                else :
                    self.canvas.move(item,
                        new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
                    x1, y1, x2, y2 = self.canvas.coords(self.p1)
                    x1 = (x1 + x2)/2
                    y1 = (y1 + y2)/2
                    x2, y2, x3, y3 = self.canvas.coords(self.p2)
                    x2 = (x3 + x2)/2
                    y2 = (y3 + y2)/2
                    self.canvas.coords(self.rect, x1, y1, x2, y2)

                self.mouse_xpos = new_xpos
                self.mouse_ypos = new_ypos
            else:
                self.move_flag = True
                self.canvas.tag_raise(item)
                self.mouse_xpos = event.x
                self.mouse_ypos = event.y
                
        return resize
    
    def deleteFromCanvas(self):
        self.canvas.delete(self.p1)
        self.canvas.delete(self.p2)
        self.canvas.delete(self.rect)
            
    def release(self, event):
        self.move_flag = False
        self.ignore_move= False
        
        

class EditableRectangleWithTimeManagement(EditableRectangle, ObjectInTime):
    def __init__(self, VideoTool, point_color = 'black', color = 'blue' , size = 10,  p1=(0, 0), p2=(20, 20), dashes = [3 , 2], invHomography=None, newCamera=None, interpkind = 'slinear'):
        ObjectInTime.__init__(self)
        EditableRectangle.__init__(self, VideoTool, point_color = point_color, color = color , size = 10,  p1=(0, 0), p2=(20, 20), dashes = [3 , 2])
        self.invHomography = invHomography
        self.newCamera = newCamera
        self.initp1 = p1
        self.initp2 = p2
        self.OvalItem = self.p1
        self.selected = False
        self.interpkind = interpkind
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Button-1>', self.select )
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Control-1>', self.selectKeep)
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Button-3>', self.select )
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem, '<Control-3>', self.selectKeep)
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem,'<Button-3>', VideoTool.B3MouseMenuContent, '+')
        VideoTool.Zoomable.canvas.tag_bind(self.OvalItem,'<Control-3>', VideoTool.B3MouseMenuContent, '+')
        
    
    def select(self, event):
            self.VideoTool.SelectPoints([self.OvalItem])
    
    def selectKeep(self, event):
            self.VideoTool.SelectPoints([self.OvalItem],True)
            
    def setSelect(self,select):
            self.selected = select
            if select:
                self.canvas.itemconfig(self.rect, width=5)
                self.canvas.tag_raise(self.p1)
                self.canvas.tag_raise(self.p2)
            else:
                self.canvas.itemconfig(self.rect, width=1)
        
    def release(self, event):
        super(EditableRectangleWithTimeManagement, self).release(event)
        x1, y1, x2, y2 =self.canvas.coords(self.rect)
        self.UpdatedPosition[self.current_frame]= (x1/self.VideoTool.Zoomable.imscale[0], y1/self.VideoTool.Zoomable.imscale[1]), (x2/self.VideoTool.Zoomable.imscale[0], y2/self.VideoTool.Zoomable.imscale[1])
        
        
    def setColor(self, color, points=True):
        if points :
            self.point_color = color
            self.canvas.itemconfig(self.p1, fill=color)
            self.canvas.itemconfig(self.p2, fill=color)
        else :
            self.color = color
            self.canvas.itemconfig(self.rect, outline=color)
            
            
    def getTimeIntervalPositionsAndVelocities(self):
        positions = moving.Trajectory()
        velocities = moving.Trajectory()
        timeInterval = moving.TimeInterval(min(self.UpdatedPosition.keys()),max(self.UpdatedPosition.keys()))
        keys = sorted(self.UpdatedPosition.keys())
        if self.invHomography is not None:
            homographie = numpy.linalg.inv(self.invHomography)
        else:
            homographie = None
        if self.newCamera is not None :
            nCamera = numpy.linalg.inv(self.newCamera)
        else :
            nCamera = None
        pos1x = interp1d(list(self.UpdatedPosition.keys()), [p[0][0] for p in self.UpdatedPosition.values()], kind=self.interpkind)(list(range(timeInterval.first, timeInterval.last+1)))
        pos1y = interp1d(list(self.UpdatedPosition.keys()), [p[0][1] for p in self.UpdatedPosition.values()], kind=self.interpkind)(list(range(timeInterval.first, timeInterval.last+1)))
        pos2x = interp1d(list(self.UpdatedPosition.keys()), [p[1][0] for p in self.UpdatedPosition.values()], kind=self.interpkind)(list(range(timeInterval.first, timeInterval.last+1)))
        pos2y = interp1d(list(self.UpdatedPosition.keys()), [p[1][1] for p in self.UpdatedPosition.values()], kind=self.interpkind)(list(range(timeInterval.first, timeInterval.last+1)))
        for i in range(len(pos1x)):
            try:
                vlc = (pos1x[i+1] - pos1x[i])/2, (pos1y[i+1] - pos1y[i])/2
                vrc = (pos2x[i+1] - pos2x[i])/2, (pos2y[i+1] - pos2y[i])/2
            except:
                vlc,vrc=(0., 0.), (0., 0.)
            positions.addPositionXY((pos1x[i] + pos2x[i] )/ 2, (pos1y[i] + pos2y[i] )/ 2)
            velocities.addPositionXY((vlc[0] + vrc[0] )/ 2, (vlc[1] + vrc[1] )/ 2)
        return timeInterval, positions.newCameraProject(nCamera).homographyProject(homographie), velocities.newCameraProject(nCamera).homographyProject(homographie)
        
    def setToFrame(self, frameNumber):
        self.current_frame=frameNumber
        widthi, heighti = self.VideoTool.Zoomable.image.size
        widthi, heighti = int(self.VideoTool.Zoomable.imscale[0] * widthi), int(self.VideoTool.Zoomable.imscale[1] * heighti)
        try:
            p1 , p2 =self.UpdatedPosition[frameNumber]
        except :
            if len(self.UpdatedPosition.value)>=2 :
                interp1x, interp1y, interp2x, interp2y = extrap1d(interp1d(list(self.UpdatedPosition.keys()), [p[0][0] for p in self.UpdatedPosition.values()], kind = self.interpkind)), extrap1d(interp1d(list(self.UpdatedPosition.keys()), [p[0][1] for p in self.UpdatedPosition.values()], kind = self.interpkind)), extrap1d(interp1d(list(self.UpdatedPosition.keys()), [p[1][0] for p in self.UpdatedPosition.values()], kind = self.interpkind)), extrap1d(interp1d(list(self.UpdatedPosition.keys()), [p[1][1] for p in self.UpdatedPosition.values()], kind = self.interpkind)) 
                p1x, p1y, p2x, p2y = interp1x(frameNumber), interp1y(frameNumber), interp2x(frameNumber), interp2y(frameNumber)
                p1 = 0 if p1x<0 else (p1x if p1x*self.VideoTool.Zoomable.imscale[0] < widthi  else widthi/self.VideoTool.Zoomable.imscale[0]) , 0 if p1y<0 else (p1y if p1y*self.VideoTool.Zoomable.imscale[1] < heighti else heighti/self.VideoTool.Zoomable.imscale[1])
                p2 = 0 if p2x<0 else (p2x if p2x*self.VideoTool.Zoomable.imscale[0] < widthi  else widthi/self.VideoTool.Zoomable.imscale[0]) , 0 if p2y<0 else (p2y if p2y*self.VideoTool.Zoomable.imscale[1] < heighti else heighti/self.VideoTool.Zoomable.imscale[1])
            
            elif len(self.UpdatedPosition.value)==1:
                p1, p2 = list(self.UpdatedPosition.values())[0]
            else:
                p1 = (0, 0) if self.initp1[0] < 0 or self.initp1[0]*self.VideoTool.Zoomable.imscale[0] > widthi or self.initp1[1] < 0 or self.initp1[1]*self.VideoTool.Zoomable.imscale[0] > heighti else self.initp1
                p2 = (widthi/self.VideoTool.Zoomable.imscale[0], heighti/self.VideoTool.Zoomable.imscale[1]) if self.initp2[0] < 0 or self.initp2[0]*self.VideoTool.Zoomable.imscale[0] > widthi or self.initp2[1] < 0 or self.initp2[1]*self.VideoTool.Zoomable.imscale[0] > heighti else self.initp2
            
            p1 , p2 = ( int(min(p1[0],p2[0])) , int(min(p1[1],p2[1]))  ) , ( int(max(p1[0],p2[0])) , int(max(p1[1],p2[1]))  )
            self.UpdatedPosition[frameNumber]=(p1, p2)
        p1 = (int(p1[0]*self.VideoTool.Zoomable.imscale[0]), int(p1[1]*self.VideoTool.Zoomable.imscale[1]) )
        p2 = (int(p2[0]*self.VideoTool.Zoomable.imscale[0]), int(p2[1]*self.VideoTool.Zoomable.imscale[1]) )
        super(EditableRectangleWithTimeManagement, self).setBoundings(p1 , p2)



