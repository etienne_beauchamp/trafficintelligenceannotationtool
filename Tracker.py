import cv2
import sys
import numpy as np
import math
import random
from trafficintelligence import moving

class Tracker:
    def __init__(self, firstFrame, frameNumber = 0, **kwargs):
        self.height, self.width, depth = firstFrame.shape
        self.startPoint = kwargs.get('startPoint', (0, 0))
        self.endPoint = kwargs.get('endPoint', (self.width, self.height))
        self.accelerationBound = kwargs.get('accelerationBound', 3)
        self.minVelocityCosine = kwargs.get('minVelocityCosine', 0.8)
        self.updateTimer = kwargs.get('updateTimer', 10)
        self.updateTime = self.updateTimer
        self.ndisplacements = kwargs.get('ndisplacements', 3)
        self.minFeatureTime = kwargs.get('minFeatureTime', 10)
        self.minFeatureDisplacement = kwargs.get('minFeatureDisplacement', 0.05)
        
        self.feature_params = dict( maxCorners = kwargs.get('maxNFeatures', 1000),
                           qualityLevel = kwargs.get('featureQuality', 0.0812219538558),
                           minDistance = kwargs.get('minFeatureDistanceKLT', 3.54964337411),
                           blockSize = kwargs.get('blockSize', 7 ),
                           useHarrisDetector = kwargs.get('useHarrisDetector', 0 ),
                           k = kwargs.get('k', 0.04 ))
        self.lk_params = dict( winSize  = (kwargs.get('windowSize', 6),kwargs.get('windowSize', 6)),
                      maxLevel = kwargs.get('pyramidLevel', 5),
                      criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, kwargs.get('maxNumberTrackingIterations', 20), kwargs.get('minTrackingError', 0.183328975142)),
                      flags = kwargs.get('featureFlags', 0),
                      minEigThreshold = kwargs.get('minFeatureEigThreshold', 1e-4) )
        zone = np.zeros((self.height,self.width), np.uint8)
        cv2.rectangle(zone, self.startPoint, self.endPoint, (255,255,255),  cv2.FILLED, 1)
        self.old_gray = cv2.cvtColor(firstFrame, cv2.COLOR_BGR2GRAY)
        self.pp0 = cv2.goodFeaturesToTrack(self.old_gray, mask = zone, **self.feature_params)
        self.pp1 = None
        self.resultList = []
        self.frameNumber = frameNumber
        self.colorCorrespondance = {}
        self.mask = np.zeros_like(firstFrame)
        self.lastStartPoint = self.startPoint
        self.lastEndPoint = self.endPoint
        self.tolerance = kwargs.get('tolerance', (50, 50))
        self.onTracking = True
        
    def update(self, nextFrame) :
        if self.onTracking :
            self.frameNumber+=1
            pointsNumber = 0
            frame_gray = cv2.cvtColor(nextFrame, cv2.COLOR_BGR2GRAY)
            try:
                self.pp1, st, err = cv2.calcOpticalFlowPyrLK(self.old_gray, frame_gray, self.pp0, None, **self.lk_params)
            except:
                self.onTracking = False
                return nextFrame
            if self.pp1 is None:
                self.onTracking = False
                return nextFrame
            good_new = self.pp1[st==1]
            good_old = self.pp0[st==1]
            self.lastStartPoint = self.startPoint if self.startPoint != (None, None) else self.lastStartPoint
            self.lastEndPoint = self.endPoint if self.endPoint != (None, None) else self.lastEndPoint
            self.startPoint = (None, None)
            self.endPoint = (None, None)
            for i,(new,old) in enumerate(zip(good_new,good_old)):
                a,b = new.ravel()
                c,d = old.ravel()
                tmp2 = [self.resultList.index(l) for l in self.resultList if l[-1]==(self.frameNumber-1,(c,d))]
                if len(tmp2)==0:
                    if a<self.lastStartPoint[0] or a>self.lastEndPoint[0] or b<self.lastStartPoint[1] or b>self.lastEndPoint[1] :
                        continue
                    self.resultList.append([(self.frameNumber-1 , (c,d))])
                    tmp2.append(len(self.resultList)-1)
                if len(tmp2)>1:
                    continue
                else:
                    #Checking And suppression
                    l = len(self.resultList[tmp2[0]])
                    tact= (a-c, b-d)
                    ntact = math.hypot(*tact)
                    if l>1 :
                        _, tpprec = self.resultList[tmp2[0]][-2]
                        tprec= (c-tpprec[0], d-tpprec[1])
                        ntprec = math.hypot(*tprec)
                        if ntprec==0 or ntact==0 or (ntact/ntprec) > self.accelerationBound :
                            continue
                        if (tprec[0]*tact[0] + tprec[1]*tact[1])/(ntprec*ntact) < self.minVelocityCosine :
                            continue
                    if l>= self.ndisplacements-1:
                        s = sum([math.hypot(self.resultList[tmp2[0]][i+1][1][0] - self.resultList[tmp2[0]][i][1][0] , self.resultList[tmp2[0]][i+1][1][1] - self.resultList[tmp2[0]][i][1][1]) for i in range(l-self.ndisplacements+1, l-1)]) + ntact
                        if s/self.ndisplacements < self.minFeatureDisplacement :
                            continue
                    self.startPoint = (a if self.startPoint[0] is None else min(self.startPoint[0],a) , b if self.startPoint[1] is None else min(self.startPoint[1],b))
                    self.endPoint = (a if self.endPoint[0] is None else max(self.endPoint[0],a) , b if self.endPoint[1] is None else max(self.endPoint[1],b) )
                    pointsNumber += 1
                    self.resultList[tmp2[0]].append((self.frameNumber,(a,b)))
                    if len(self.resultList[tmp2[0]]) < self.minFeatureTime :
                        continue
                    if len(self.resultList[tmp2[0]]) == self.minFeatureTime:
                        self.colorCorrespondance[tmp2[0]] = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
                        de, ar = self.resultList[tmp2[0]][0][1], self.resultList[tmp2[0]][1][1]
                        self.mask = cv2.line(self.mask, de, ar, self.colorCorrespondance[tmp2[0]], 2)
                        for (_,de),(_,ar) in zip(self.resultList[tmp2[0]][:-1], self.resultList[tmp2[0]][1:]):
                            self.mask = cv2.line(self.mask, de, ar, self.colorCorrespondance[tmp2[0]], 2)
                    else :
                        self.mask = cv2.line(self.mask, (a, b), (c, d), self.colorCorrespondance[tmp2[0]], 2)
                        nextFrame = cv2.circle(nextFrame, (a, b), 5, self.colorCorrespondance[tmp2[0]], -1)
            
            if pointsNumber==0:
                self.onTracking = False
            nextFrame = cv2.add(nextFrame, self.mask)
            self.old_gray = frame_gray.copy()
            self.pp0 = good_new.reshape(-1,1,2)
            self.updateTime -= 1
            if self.updateTime ==0 :
                self.correctBounding()
        return nextFrame
      
    def getBounding(self):
        return ( self.lastStartPoint if self.startPoint == (None,None) else self.startPoint , self.lastEndPoint if self.endPoint == (None,None) else self.endPoint)
      
    def correctBounding(self, startPoint= None, endPoint = None):
        if startPoint is not None:
            self.startPoint = startPoint
        elif self.startPoint == (None,None):
            self.startPoint = self.lastStartPoint
        if endPoint is not None:
            self.endPoint = endPoint
        elif self.endPoint == (None,None):
            self.endPoint = self.lastEndPoint
        zone = np.zeros((self.height,self.width), np.uint8)
        cv2.rectangle(zone, self.startPoint, self.endPoint, (255,255,255),  cv2.FILLED, 1)
        tmp2 = [l[-1][1] for l in self.resultList if l[-1][0]==self.frameNumber]
        for x,y in tmp2:
            cv2.rectangle(zone, (int(x-self.feature_params['minDistance']/2) , int(y-self.feature_params['minDistance']/2)), ( int(x+self.feature_params['minDistance']/2) , int(y+self.feature_params['minDistance']/2)), (255,255,255),  cv2.FILLED, 1)
        pp0 = cv2.goodFeaturesToTrack(self.old_gray, mask = zone, **self.feature_params)
        try:
            tmp = np.concatenate([self.pp0,pp0])
        except ValueError:
            return
        self.pp0 = tmp
        self.updateTime = self.updateTimer
        
    def generateObject(self, homography=None, newCameraMatrix=None, offsetFeatureNumber = 10, objectNum = 10, userType = moving.userType2Num['unknown']):
        if newCameraMatrix is not None :
            nCamera = np.linalg.inv(newCameraMatrix)
        else :
            nCamera = None
        def list2feature(li, num):
            t = moving.TimeInterval(li[0][0], li[-1][0])
            p = moving.Trajectory()
            for _, (x, y) in li:
                p.addPositionXY(x, y)
            p = p.newCameraProject(nCamera).homographyProject(homography)
            v = moving.Trajectory()
            for i, p1 in enumerate(p):
                try :
                    p2 = p[i+1]
                except :
                    p2 = p1
                v.addPosition((p2-p1)*0.5)
            return moving.MovingObject(num, t, p, v)
        
        
        realfeatureList = [i for i in self.resultList if len(i)>=self.minFeatureTime]
        stats = {t : sum([t>=obj[0][0] and t<=obj[-1][0] for obj in self.resultList]) for t in range(min([obj[0][0] for obj in self.resultList]), max([obj[-1][0] for obj in self.resultList])+1) }
        holes = [t for t in stats if stats[t]==0]
        candidats = [[i for i in self.resultList if i not in realfeatureList and t in range(i[0][0], i[-1][0]+1)] for t in holes]
        candidats = [max(lis, key= lambda x : len(i) ) for lis in candidats]
        realfeatureList += list(set(candidats))
        featurelist = []
        for i,o in enumerate(realfeatureList):
            featurelist.append(list2feature(o, i+offsetFeatureNumber))
        if featurelist == []:
            return None
        else:
           t,p,v  = moving.MovingObject.aggregateTrajectories(featurelist)
           result = moving.MovingObject(objectNum, t, p, v, userType = userType)
           result.features = featurelist
           result.featureNumbers = list(range(offsetFeatureNumber, offsetFeatureNumber + len(featurelist)))
           return result
        
