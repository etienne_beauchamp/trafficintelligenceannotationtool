import copy
import undoredo
from trafficintelligence import moving


class List(object):
    def __init__(self, objectList=[], saved = True):
        self.undo_mgr = undoredo.UndoManager()
        self.value = list(objectList)
        self.saved = saved
    
    @undoredo.undoable
    def concatenateWith(self, objIdx , objIdx2, featureOffset, saved = False):
        obj = self.value[objIdx]
        nobj = moving.MovingObject.concatenate(obj, self.value[objIdx2], newFeatureNum = featureOffset)
        self.value[objIdx] = nobj
        del self.value[objIdx2]
        if hasattr(nobj,'projectedPositions'):
            delattr(nobj,'projectedPositions')
        self.saved = saved
    
    @undoredo.undoable
    def createObjectByFeatures(self, Features, Num, userType, featureOffset, saved = False):
        Features = sorted(Features, key = (lambda x : (x.getFirstInstant(), 0 if x.timeInterval is None else -x.length()))) 
        FeaturesGroup = []
        for obj1 in Features:
            found = False
            for idx, (i, List) in enumerate(FeaturesGroup):
                if not moving.Interval.intersection(i, obj1.getTimeInterval()).empty():
                    FeaturesGroup[idx] = (moving.Interval.union(obj1.getTimeInterval(), i), List+[obj1])
                    found = True
                    break
            if not found:
                FeaturesGroup.append((obj1.getTimeInterval(), [obj1]))
        for _, Features in FeaturesGroup:
            inter, positions, velocities = moving.MovingObject.aggregateTrajectories(Features)
            nobj = moving.MovingObject( num = Num, timeInterval = inter, positions = positions, velocities = velocities, userType = userType )
            Num+=1
            nobj.features = []
            nobj.featureNumbers = []
            for i,f in enumerate(Features) :
                tmp = copy.deepcopy(f)
                tmp.num = featureOffset+i
                nobj.features.append(tmp)
                nobj.featureNumbers.append(featureOffset+i)
                featureOffset+=1
            self.value.append(nobj)
        self.saved = saved
        return len(FeaturesGroup)
    
    @undoredo.undoable
    def cropObject(self, idx, value, after, saved = False): 
        self.value[idx] = moving.MovingObject.croppedTimeInterval(self.value[idx], value, after)
        self.saved = saved
        
    @undoredo.undoable
    def splitObject(self, idx, value, objId, FeatureNumOffset, saved = False): 
        obj = self.value[idx]
        self.value[idx] = moving.MovingObject.croppedTimeInterval(obj, value, True)
        self.value.insert(idx+1, moving.MovingObject.croppedTimeInterval(obj, value, False))
        self.value[idx+1].num = objId
        if self.value[idx+1].hasFeatures():
            self.value[idx+1].featureNumbers = []
            for i, f in enumerate(self.value[idx+1].features):
                f.num = FeatureNumOffset+i
                self.value[idx+1].featureNumbers.append(FeatureNumOffset+i)
        self.saved = saved
    
    @undoredo.undoable
    def delObjects(self, idxList, saved = False):
        if len(idxList)>0:
            self.value = [o for i,o in enumerate(self.value) if i not in idxList]
            self.saved = saved
        
    @undoredo.undoable
    def ReplaceList(self, other, saved = False):
        self.value=other
        self.saved = saved
        
    def delFeatures(self, FeaturesTupleList, featureOffset, saved = False):
        newList = []
        modified = False
        for idx, obj in enumerate(self.value):
            Features=[t[0] for t in FeaturesTupleList if t[1]==obj]
            if len(Features)>0:
                nobj = copy.deepcopy(obj)
                nobj.features = [f for f in obj.features if f not in Features]
                nobj.featureNumbers = [obj.featureNumbers[i] for i in range(len(obj.featureNumbers)) if obj.features[i] in nobj.features]
                if nobj.features == []:
                    nobj.features = [copy.deepcopy(nobj)]
                    nobj.featureNumbers = [featureOffset]
                    featureOffset+=1
                else:
                    inter, positions, velocities = moving.MovingObject.aggregateTrajectories(nobj.features)
                    for i in range(positions.length()):
                        try:
                            positions[i].asint()
                        except:
                            raise ValueError('The deletion of the feature(s) {} for object #{} will create an empty value at frame no {}. The object exists from {} to {}. \nSafe solution\n-------------\nSolution 1 : Make the object start from {} to {}.\n\nNon-checked solution : the problem can occur at a later frame\n-------------------------------------------------------------\nSolution 2 : keep at least one of them. \nSolution 3 : Make the object start from {} to {}.'.format(["#"+str(f.getNum()) for f in Features if f.existsAtInstant(inter.first + i)], obj.getNum(), i+inter.first, inter.first, inter.last, inter.first, inter.first + i, inter.first + i, inter.last))
                    nobj.timeInterval=inter
                    nobj.positions=positions
                    nobj.velocities=velocities
                if hasattr(nobj,'projectedPositions'):
                    delattr(nobj,'projectedPositions')
                newList.append(nobj)
                modified = True
            else:
                newList.append(obj)
        if modified :
            self.ReplaceList(newList, saved)
    
    def __len__(self):
            return len(self.value)
    
    @undoredo.undoable
    def __setitem__(self,key,value):
        self.value[key]=value
        self.saved = False
        
    def setSaved(self):
        self.saved = True
        
    @undoredo.undoable
    def __delitem__(self, key):
        del self.value[key]
        self.saved = False
    
    @undoredo.undoable
    def __iadd__(self, other):
        self.value+=other
        self.saved = False
        return self

    def __getitem__(self, key):
        return self.value[key]
        
    def __contains__(self, item):
        return item in self.value
        
    def do(self, command):
        return self.undo_mgr.do(command)

    def undo(self):
        return self.undo_mgr.undo()

    def redo(self):
        return self.undo_mgr.redo()

    def copy(self):
        return List(self.value, self.saved)

    def can_undo(self):
        return self.undo_mgr.can_undo()
        
    def can_redo(self):
        return self.undo_mgr.can_redo()

    def restore(self, counter):
        self.value = counter.value
        self.saved = counter.saved
